<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='product';
		$this->id_field = 'id';
		$this->row_type = 'product_object';
		
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('is_active',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('is_active',0);
		return $this;
	}

	public function join(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('category_product', $this->table.'.category_id = category_product.category_id', 'left');
		$query = $this->db->get();

		return $query->result();
	}

	
}


	class product_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 