<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='tb_users';
		$this->id_field = 'id';
		$this->row_type = 'Users_object';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('active',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('active',0);
		return $this;
	}

	public function join(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('tb_groups', 'tb_groups.group_id = tb_users.group_id','LEFT');
		$query = $this->db->get();

		return $query->result();
	}
}


	class Users_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 