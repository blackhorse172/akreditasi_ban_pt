<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PengelolaProgramStudies_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='aps_pengelola_program_studi';
		$this->id_field = 'id';
		$this->row_type = 'aps_pengelola_program_studi_object';
		
		parent::__construct();
	}

	// public function isActive(){
	// 	$this->db->where('is_active',1);
	// 	return $this;
	// }

	// public function noActive(){
	// 	$this->db->where('is_active',0);
	// 	return $this;
	// }
	
}


	class aps_pengelola_program_studi_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 