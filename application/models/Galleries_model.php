<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Galleries_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='gallery';
		$this->id_field = 'gallery_id';
		$this->row_type = 'Gallery_object';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Gallery_object';*/
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('status',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('status',0);
		return $this;
	}
	public function join(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('category_gallery', $this->table.'.category_id = category_gallery.category_id', 'left');
		$query = $this->db->get();

		return $query->result();
	}
}


	class Gallery_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 