<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ArticleCategories_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table = 'tb_blogcategories';
		$this->id_field = 'CatID';
		$this->row_type = 'ArticleCategory_object';
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('enable','1');
		return $this;
	}

	public function noActive(){
		$this->db->where('enable','0');
		return $this;
	}
}


	class ArticleCategory_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 