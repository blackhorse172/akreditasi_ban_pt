<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='tb_blogs';
		$this->row_type= 'Article_object';
		$this->id_field = 'blogID';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function hasPublish(){
		$this->db->where('status','publish');
		return $this;
	}

	public function noPublish(){
		$this->db->where('status','unpublish');
		return $this;
	}
}


	class Article_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 