<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Articles');
		$this->load->model('ArticleCategories');
		
	}
	

	public function index()
	{
		$data = $this->Articles->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('article/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create()
	{
		$data = array(
			'id' => 1,
			'is_active' => 1,
			'title' => "Test Article",
			'is_publish' => 1,
			'value' => "<p> hello world </p>",
			'image' => "assets/metronic/app/media/img/blog/blog1.jpg",
		);
		$this->data['category'] = $this->ArticleCategories->isActive()->get_list();
		$this->data['isEdit'] = false;
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('article/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Articles->get($id);
		$this->data['category'] = $this->ArticleCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('article/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		$fileName = '';
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/blog/title_image/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Article/Create');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		
		$article = $this->Articles->new_row();

		$article->CatID = $this->input->post('CatID');
		$article->title = $this->input->post('title');	
		$article->slug =  str_replace(' ','-',$this->input->post('title'));	
		$article->content = $this->input->post('content');	
		$article->created = date('Y-m-d H:i:s');
		$article->entryby = $this->session->userdata('userName');
		$article->status = $this->input->post('publish');	
		$article->image = $fileName;	
		//$this->debug($article);
		$id = $article->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Article',301);
	}

	public function saveEdit()
	{
		$fileName = '';
		$id = $this->input->post('id');
		$article = $this->Articles->get($id);
		
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug(count($this->input->post($_FILES["image"])));
			$config['upload_path'] = './files/blog/title_image/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
					//unlink exist data
					if($article->image != '' || $article->image != null){
						$image = $config['upload_path'].$article->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('Article/Create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$article->image = $fileName;

						}
			}
			
		}
		$article->CatID = $this->input->post('CatID');
		$article->title = $this->input->post('title');	
		$article->slug =  str_replace(' ','-',$this->input->post('title'));	
		$article->content = $this->input->post('content');
		$article->status = $this->input->post('publish');	
		$id = $article->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Article',301);
	}

	public function nonActive($id)
	{
		$article = $this->Articles->get($id);

		if($article->status == 'publish'){
			$article->status = 'unpublish';
 		} else {
			$article->status = 'publish';
		 }

		 $article->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Article',301);
	}

	public function categoryNonActive($id)
	{
		$article = $this->ArticleCategories->get($id);

		if($article->enable == '1'){
			$article->enable = '0';
 		} else {
			$article->enable = '1';
		 }

		 $article->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Article/category',301);
	}

	public function category()
	{
		$data = $this->ArticleCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('article/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('article/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->ArticleCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Article/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('article/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->ArticleCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->ArticleCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->name = $this->input->post('name');
		$category->slug =  str_replace(' ','-',$this->input->post('name'));
		$category->enable =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Article/category',301);
	}

	public function uploadImage()	{
		if(isset($_FILES["image"]["name"]))
		{
			
			$config['upload_path'] = './files/blog/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			//$this->debug($this->upload->do_upload("image"));
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				echo json_encode($error);
			}
			else{
				
				$data = $this->upload->data();
					//Compress Image
					$config['image_library']='gd2';
					$config['source_image']='./assets/images/'.$data['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= TRUE;
					$config['quality']= '60%';
					$config['width']= 800;
					$config['height']= 800;
					$config['new_image']= './files/blog/'.$data['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					echo base_url().'files/blog/'.$data['file_name'];
			}
		}
		
	}

	public function deleteImage(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		
		if(unlink($file_name)){
			echo 'File Delete Successfully';
		}
	}



}

/* End of file Article.php */
