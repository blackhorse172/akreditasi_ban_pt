<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users');
		$this->load->model('UserGroups');
		
		//Do your magic here
	}
	

	public function index()
	{
		$data = $this->Users->join();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('users/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create()
	{
		$data = array(
			'id' => 1,
			'is_active' => 1,
			'title' => "Admin",
			'is_publish' => 1,
			'value' => "<p> hello world </p>",
			'image' => "assets/metronic/app/media/img/blog/blog1.jpg",
		);
		$group = $this->UserGroups->where_in('group_id',array(1,2,3))->get_list();
		$this->data['isEdit'] = false;
		$this->data['group'] = $group;
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('users/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Users->get($id);
		if($data == '')
		{
			redirect('error/notFound');
		}
		//$this->debug($data);
		$group = $this->UserGroups->where_in('group_id',array(1,2,3))->get_list();
		$this->data['isEdit'] = true;
		$this->data['group'] = $group;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('users/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{
		//$this->debug($this->input->post());
		$users = $this->Users->new_row();

		$users->username = $this->input->post('username');
		$users->first_name = $this->input->post('first_name');	
		$users->last_name =  $this->input->post('last_name');
		$users->email = $this->input->post('email');	
		$users->password = md5(trim($this->input->post('password')));
		$users->group_id = $this->input->post('group_id');
		$users->active = 1;	

		$id = $users->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Users',301);
	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$users = $this->Users->get($id);
		//$this->debug($this->input->post());
		if($this->input->post('password') != '') $users->password = md5(trim($this->input->post('password')));

		$users->username = $this->input->post('username');
		$users->first_name = $this->input->post('first_name');	
		$users->last_name =  $this->input->post('last_name');
		$users->email = $this->input->post('email');	
		$users->group_id = $this->input->post('group_id');
		$users->updated_at = date('Y-m-d H:i:s');	

		$id = $users->save();
		SiteHelpers::alert('success'," Data has Edited succesfuly !");

		redirect('Users',301);
	}

	public function getExistEmail()
	{
		$email = $this->input->post('email');
		
		$res = false;
		$this->db->where('email', $email);
		$this->db->from('tb_users');
		$q = $this->db->get();
		
		if($q->num_rows() > 0) $res = true;

		echo json_encode($res);
		
	}

	public function nonActive($id)
	{
		$article = $this->Users->get($id);

		if($article->active == 1){
			$article->active = 0;
 		} else {
			$article->active = 1;
		 }

		 $article->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Users',301);
	}

	public function category()
	{
		$data = $this->UserGroups->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('users/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('users/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->UserGroups->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Users/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('users/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->UserGroups->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->UserGroups->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->name = $this->input->post('name');
		$category->description =  $this->input->post('desc');
		$category->is_active =  1;

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Users/category',301);
	}

	public function categoryNonActive($id)
	{
		$users = $this->UserGroups->get($id);

		if($users->is_active == 1){
			$users->is_active = 0;
 		} else {
			$users->is_active = 1;
		 }

		 $users->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Users/category',301);
	}

}

/* End of file slider.php */
