<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
    {
        parent::__construct();    
    if(!$this->session->userdata('logged_in')) redirect('user/login',301);      
    }

	public function index()
	{
		$this->data['content'] = $this->load->view('dashboard/Index',$this->data,true);    
    $this->load->view('layouts/main',$this->data);
	}

}

/* End of file Controllername.php */
