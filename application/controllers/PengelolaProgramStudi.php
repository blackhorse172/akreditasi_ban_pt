<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PengelolaProgramStudi extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('PengelolaProgramStudies');
		
	}
	

	public function index()
	{
		$data = $this->PengelolaProgramStudies->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('PengelolaProgramStudi/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create($id = null)
	{
		$data = array();
		if($id != null) {
			$data = $this->PengelolaProgramStudies->get($id);
			$data->tgl_sk = date("Y-m-d",strtotime($data->tgl_sk));
			$data->tgl_kadaluarsa = date("Y-m-d",strtotime($data->tgl_kadaluarsa));

			//$this->debug($data);
			$this->data['isEdit'] = true;

		} else {
			$this->data['isEdit'] = false;

		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('PengelolaProgramStudi/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save($id = null)
	{
		
		$data = $this->input->post();

		if($id == null) {
			$data["tahun"] = date("Y");
			//$this->debug($data);

			$this->db->insert('aps_pengelola_program_studi', $data);
			
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			redirect("PengelolaProgramStudi",301);

		} else {
			
			$this->db->where('id', $id);
			$this->db->update('aps_pengelola_program_studi', $data);
			
			SiteHelpers::alert('success'," Data has been Edit succesfuly !");
			redirect("PengelolaProgramStudi",301);

		}
		// $this->data['data'] = json_encode($data);
		// $this->data['content'] = $this->load->view('PengelolaProgramStudi/create',$this->data,true);    
    	// $this->load->view('layouts/main',$this->data);
	}



}

/* End of file PengelolaProgramStudi.php */
