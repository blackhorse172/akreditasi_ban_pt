<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Pages');
		//$this->load->model('PageCategories');
		
	}
	

	public function index()
	{
		$data = $this->Pages->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('pages/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create()
	{
		$data = array(
			'id' => 1,
			'is_active' => 1,
			'title' => "Test Pages",
			'is_publish' => 1,
			'value' => "<p> hello world </p>",
			'image' => "assets/metronic/app/media/img/page/blog1.jpg",
		);
		//$this->data['category'] = $this->PageCategories->isActive()->get_list();
		$this->data['isEdit'] = false;
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('pages/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Pages->get($id);
		//$this->data['category'] = $this->PageCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('pages/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		
		$pages = $this->Pages->new_row();

		$pages->title = $this->input->post('title');
		$pages->content = $this->input->post('content');	
		$pages->created = date('Y-m-d H:i:s');

		//$this->debug($pages);
		$id = $pages->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Pages',301);
	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$pages = $this->Pages->get($id);
		
		

		$pages->title = $this->input->post('title');
		$pages->content = $this->input->post('content');	
		$pages->updated = date('Y-m-d H:i:s');

		$id = $pages->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Pages',301);
	}

	public function nonActive($id)
	{
		$pages = $this->Pages->get($id);

		if($pages->status == 'publish'){
			$pages->status = 'unpublish';
 		} else {
			$pages->status = 'publish';
		 }

		 $pages->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Pages',301);
	}

	public function categoryNonActive($id)
	{
		$pages = $this->PageCategories->get($id);

		if($pages->enable == '1'){
			$pages->enable = '0';
 		} else {
			$pages->enable = '1';
		 }

		 $pages->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Pages/category',301);
	}

	public function category()
	{
		$data = $this->PageCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('pages/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('pages/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->PageCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Pages/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('pages/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->PageCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->PageCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->name = $this->input->post('name');
		$category->slug =  str_replace(' ','-',$this->input->post('name'));
		$category->enable =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Pages/category',301);
	}

	public function uploadImage()	{
		if(isset($_FILES["image"]["name"]))
		{
			
			$config['upload_path'] = './files/page/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			//$this->debug($this->upload->do_upload("image"));
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				echo json_encode($error);
			}
			else{
				
				$data = $this->upload->data();
					//Compress Image
					$config['image_library']='gd2';
					$config['source_image']='./assets/images/'.$data['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= TRUE;
					$config['quality']= '60%';
					$config['width']= 800;
					$config['height']= 800;
					$config['new_image']= './files/page/'.$data['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					echo base_url().'files/page/'.$data['file_name'];
			}
		}
		
	}

	public function deleteImage(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		
		if(unlink($file_name)){
			echo 'File Delete Successfully';
		}
	}



}

/* End of file Pages.php */
