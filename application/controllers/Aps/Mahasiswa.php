<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		//$this->load->model('Kerjasamas');
	}
	

	public function index()
	{
		//$this->db->where_not_in('name',);
		$data = $this->db->get('aps_mahasiswa_baru')->result();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('aps/mahasiswa/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}


	public function asing()
	{
		//$this->db->where_not_in('name',);
		$data = $this->db->get('aps_mahasiswa_asing')->result();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('aps/mahasiswa_asing/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}


	public function create_asing($id = null)
	{
		$data = array();
		if($id != null) {
			//$data = $this->Kerjasamas->get($id);
			
			//$this->debug($data);
			$this->data['isEdit'] = true;

		} else {
			$this->data['isEdit'] = false;

		}
		$this->data['data'] = [];//$data;
		$this->data['tahunAkreditasi']= $this->db->get('master_tahun_akademik')->result_array();
		$this->data['content'] = $this->load->view('aps/mahasiswa_asing/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create($id = null)
	{
		$data = array();
		if($id != null) {
			//$data = $this->Kerjasamas->get($id);
			
			//$this->debug($data);
			$this->data['isEdit'] = true;

		} else {
			$this->data['isEdit'] = false;

		}
		$this->data['data'] = [];//$data;
		$this->data['tahunAkreditasi']= $this->db->get('master_tahun_akademik')->result_array();
		$this->data['content'] = $this->load->view('aps/Mahasiswa/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}


	public function Save($id = null)
	{
		
		$data = $this->input->post();

		if($id == null) {
			$data["tahun_akreditasi"] = date("Y");
			//$this->debug($data);

			$this->db->insert('aps_mahasiswa_baru', $data);
			
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			redirect("Aps/Mahasiswa",301);

		} else {
			
			$this->db->where('id', $id);
			$this->db->update('aps_mahasiswa_baru', $data);
			
			SiteHelpers::alert('success'," Data has been Edit succesfuly !");
			redirect("Aps/Mahasiswa",301);


		}
		// $this->data['data'] = json_encode($data);
		// $this->data['content'] = $this->load->view('PengelolaProgramStudi/create',$this->data,true);    
    	// $this->load->view('layouts/main',$this->data);
	}


	public function Save_asing($id = null)
	{
		
		$data = $this->input->post();

		if($id == null) {
			$data["tahun_akreditasi"] = date("Y");
			//$this->debug($data);

			$this->db->insert('aps_mahasiswa_asing', $data);
			
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			redirect("Aps/Mahasiswa/asing",301);

		} else {
			
			$this->db->where('id', $id);
			$this->db->update('aps_mahasiswa_asing', $data);
			
			SiteHelpers::alert('success'," Data has been Edit succesfuly !");
			redirect("Aps/Mahasiswa/asing",301);


		}
		// $this->data['data'] = json_encode($data);
		// $this->data['content'] = $this->load->view('PengelolaProgramStudi/create',$this->data,true);    
    	// $this->load->view('layouts/main',$this->data);
	}

}

/* End of file Mahasiswa.php */
