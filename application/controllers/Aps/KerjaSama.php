<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class KerjaSama extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Kerjasamas');
		
	}
	

	public function index()
	{
		$data = $this->db->get('aps_kerjasama')->result();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('aps/kerjasama/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create($id = null)
	{
		$data = array();
		if($id != null) {
			$data = $this->Kerjasamas->get($id);
			
			//$this->debug($data);
			$this->data['isEdit'] = true;

		} else {
			$this->data['isEdit'] = false;

		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('aps/kerjasama/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save($id = null)
	{
		
		$data = $this->input->post();

		if($id == null) {
			//$data["tahun"] = date("Y");
			//$this->debug($data);

			$this->db->insert('aps_kerjasama', $data);
			
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			redirect("Aps/KerjaSama",301);

		} else {
			
			$this->db->where('id', $id);
			$this->db->update('aps_kerjasama', $data);
			
			SiteHelpers::alert('success'," Data has been Edit succesfuly !");
			redirect("Aps/KerjaSama",301);


		}
		// $this->data['data'] = json_encode($data);
		// $this->data['content'] = $this->load->view('PengelolaProgramStudi/create',$this->data,true);    
    	// $this->load->view('layouts/main',$this->data);
	}

}

/* End of file KerjaSama.php */
