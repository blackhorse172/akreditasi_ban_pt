<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Users";
$action = base_url().'Users/save';
if($isEdit == true){
$action = base_url().'Users/saveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Username</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="username" id="username" aria-describedby="emailHelp" placeholder="Enter Username">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" class="form-control m-input" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter Email" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">First Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="first_name" id="first_name" aria-describedby="emailHelp" placeholder="Enter First Name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Last Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="last_name" id="last_name" aria-describedby="emailHelp" placeholder="Enter Last Name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Password</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="password" class="form-control m-input" id="password" name="password" aria-describedby="emailHelp" placeholder="Enter Password" required>
										</div>
									</div>
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Profile Photo</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="file" class="form-control m-input" id="exampleInputEmail1">
										</div>
									</div> -->
								</div>
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Group</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="group_id" id="group_id" required>
												<option></option>
												<?php foreach($group as $item) : ?>
												<option value="<?= $item->group_id ?>"><?= $item->name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Users'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<script>

$(document).ready(function () {
	
	$('#email').change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>users/getExistEmail",
			data: { email : $('#email').val() },
			dataType: "json",
			success: function (response) {
				if(response){
					swal({
						title: 'Exist',
						text: 'Email Has exist',
						type: "warning",
					});
					$('#email').focus();
					$('#email').val('');
					
				}
			}
		});
	});
});
</script>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#username").val("<?= $data->username; ?>");
	$("#email").val("<?= $data->email; ?>");
	$("#first_name").val("<?= $data->first_name; ?>");
	$("#last_name").val("<?= $data->last_name; ?>");
	$("#group_id").val("<?= $data->group_id; ?>");

	$("#username").attr('readonly',true);
	$("#email").attr('readonly',true);
	$("#password").removeAttr('required');




});
</script>
<?php endif ?>
