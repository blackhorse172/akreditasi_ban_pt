<?php
$title = "Tambah";
$action = base_url().'Aps/KerjaSama/Save';
if($isEdit == true){
$action = base_url().'Aps/KerjaSama/Save/'.$data->id;
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Lembaga</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="lembaga" class="form-control m-input" id="lembaga">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tingkat</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<!-- <input type="text" > -->
											<select name="tingkat" class="form-control m-input" id="tingkat">
												<option value=""></option>
												<option value="international">INTERNATIONAL</option>
												<option value="national">NATIONAL</option>
												<option value="lokal">LOKAL</option>

											</select>
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Judul Kerjasama</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="judul" class="form-control m-input" id="judul">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Manfaat Bagi PS Yang di Akreditasi</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="input" name="manfaat" class="form-control m-input" id="manfaat">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal Mulai</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="start_date" class="form-control m-input" id="start_date">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal Berakhir</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="end_date" class="form-control m-input" id="end_date">
										</div>
									</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bukti Kerjasama</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="bukti" class="form-control m-input" id="bukti">
										</div>
									</div>
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jumlah Mahasiswa</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="jumlah_mahasiswa" class="form-control m-input" id="jumlah_mahasiswa">
										</div>
									</div> -->
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tahun</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="tahun" class="form-control m-input" id="tahun">
										</div>
									</div> -->
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image thumbnail</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/blog/title_image/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div> -->
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Article'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#lembaga").val("<?= $data->lembaga; ?>");
	$("#tingkat").val("<?= $data->tingkat; ?>");
	$("#judul").val("<?= $data->judul; ?>");
	$("#manfaat").val("<?= $data->manfaat; ?>");
	$("#start_date").val("<?= $data->start_date; ?>");
	$("#end_date").val("<?= $data->end_date; ?>");
	$("#bukti").val("<?= $data->bukti; ?>");





});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Article/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Article/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
