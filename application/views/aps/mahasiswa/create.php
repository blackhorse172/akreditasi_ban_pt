<?php
$title = "Tambah";
$action = base_url().'Aps/Mahasiswa/Save';
if($isEdit == true){
$action = base_url().'Aps/Mahasiswa/Save/'.$data->id;
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tahun Akademik</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<!-- <input type="text" > -->
											<select name="tahun_akademik" class= "form-control m-input" id="tahun_akademikat" required>
												<option value=""></option>
												<?php foreach ($tahunAkreditasi as $key => $value) : ?>
													<option value="<?= $value['name'] ?>"><?= $value['name'] ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Daya Tampung</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="daya_tampung" class="form-control m-input" id="daya_tampung">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">calon mahasiswa pendaftar</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="calon_mahasiswa_pendaftar" class="form-control m-input" id="calon_mahasiswa_pendaftar">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">calon mahasiswa seleksi</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="calon_mahasiswa_seleksi" class="form-control m-input" id="calon_mahasiswa_seleksi">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mahasiswa baru reguler</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="mahasiswa_baru_reguler" class="form-control m-input" id="mahasiswa_baru_reguler">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mahasiswa_baru_transfer</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="mahasiswa_baru_transfer" class="form-control m-input" id="mahasiswa_baru_transfer">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mahasiswa_aktif_reguler</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="mahasiswa_aktif_reguler" class="form-control m-input" id="mahasiswa_aktif_reguler">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mahasiswa_aktif_transfer</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="mahasiswa_aktif_transfer" class="form-control m-input" id="mahasiswa_aktif_transfer">
										</div>
									</div>
									
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Article'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#lembaga").val("<?= $data->lembaga; ?>");
	$("#tingkat").val("<?= $data->tingkat; ?>");
	$("#judul").val("<?= $data->judul; ?>");
	$("#manfaat").val("<?= $data->manfaat; ?>");
	$("#start_date").val("<?= $data->start_date; ?>");
	$("#end_date").val("<?= $data->end_date; ?>");
	$("#bukti").val("<?= $data->bukti; ?>");





});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Article/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Article/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
