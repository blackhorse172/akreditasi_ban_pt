<?php
$title = "Create PPS";
$action = base_url().'PengelolaProgramStudi/Save';
if($isEdit == true){
$action = base_url().'PengelolaProgramStudi/Save/'.$data->id;
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jenis Program</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="jenis_program" class="form-control m-input" id="jenis_program">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama Program</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="nama_program" class="form-control m-input" id="nama_program">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Status</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="status" class="form-control m-input" id="status">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">No SK</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="no_sk" class="form-control m-input" id="no_sk">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal SK</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="tgl_sk" class="form-control m-input" id="tgl_sk">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal Kadaluarsa</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="tgl_kadaluarsa" class="form-control m-input" id="tgl_kadaluarsa">
										</div>
									</div>
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jumlah Mahasiswa</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="jumlah_mahasiswa" class="form-control m-input" id="jumlah_mahasiswa">
										</div>
									</div> -->
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tahun</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="tahun" class="form-control m-input" id="tahun">
										</div>
									</div> -->
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image thumbnail</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/blog/title_image/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div> -->
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Article'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#jenis_program").val("<?= $data->jenis_program; ?>");
	$("#nama_program").val("<?= $data->nama_program; ?>");
	$("#status").val("<?= $data->status; ?>");
	$("#no_sk").val("<?= $data->no_sk; ?>");
	$("#tgl_sk").val("<?= $data->tgl_sk; ?>");
	$("#tgl_kadaluarsa").val("<?= $data->tgl_kadaluarsa; ?>");
	$("#jumlah_mahasiswa").val("<?= $data->jumlah_mahasiswa; ?>");





});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Article/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Article/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
