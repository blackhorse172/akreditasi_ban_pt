<link href="<?= base_url() ?>assets/metronic/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/metronic/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
<!--begin::Global Theme Bundle -->
<script src="<?= base_url() ?>assets/metronic/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/metronic/default/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->
<link rel="shortcut icon" href="<?= base_url() ?>favicon.ico" />
<style>
	@media screen and (max-width: 968px) {
    .logomain {
        width: 25%;
    }
}

@media only screen and (max-width: 600px) {
    .logomain {
        width: 50%;
        margin-left: 0%;
    }
}

@media (min-width: 1200px) {
    .logomain {
        width: 100%;
        margin-left: 40%;
    }
}
</style>
