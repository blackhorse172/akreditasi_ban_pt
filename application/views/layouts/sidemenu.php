
<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item " aria-haspopup="true">
            <a onclick="#" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Dashboard</span>
                        <!-- @*<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span>*@ -->
                    </span>
                </span>
            </a>
		</li>

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file-1"></i><span
									 class="m-menu__link-text">LED</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Article" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List LED</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Article/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category LED</span></a></li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file-1"></i><span
									 class="m-menu__link-text">APS</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>PengelolaProgramStudi" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">PPS</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Aps/KerjaSama" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Kerja Sama</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Aps/Mahasiswa" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Mahasiswa Baru</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Aps/Mahasiswa/asing" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Mahasiswa Asing</span></a></li>

									</ul>
								</div>
							</li>

		
		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-users"></i><span
									 class="m-menu__link-text">Users</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Users" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Users</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Users/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Users</span></a></li>
									</ul>
								</div>
							</li>


							
        
    </ul>
</div>
